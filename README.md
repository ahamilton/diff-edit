# Diff-edit

## Summary

Edit two files side by side, showing differences.

## Installation

Install diff-edit directly using pipx:

    pipx install git+https://gitlab.com/ahamilton/diff-edit

Or install from source:

    git clone https://gitlab.com/ahamilton/diff-edit
    cd diff-edit
    ./install

Then to run:

    diff-edit -h
